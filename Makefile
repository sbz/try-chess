run:
	python main.py

test:
	python -m unittest discover . -v

lint:
	for f in *.py; do \
		pylint-2.7 $$f; \
	done

coverage:
	coverage run tests.py
	coverage report *.py
	coverage annotate main.py piece.py board.py
	less *.py,cover

deps:
	pip install -r requirements.txt --upgrade

clean:
	rm -rf *.pyc *.pyo *.py,cover .coverage
