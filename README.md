Try Chess
=========

Chess board implemented in Python

Run it
------

$ make run

User stories
------------

    US 01 Implement Board
        - M x N Matrix
        - Empty it
        - Create  Board create_board(m, n)
        - Display Board

    US 02 Implement Pieces and Moves
        - (K)ing
        - (Q)ueen
        - (B)ishop
        - (R)ook
        - K(N)ight

    US 03 Implement symmetry algorithms
        - Input Board + Pieces -> Output Board with symmetry

    US 04 Solve algorithms 
        - 3x3 Use Case 2K, 1R
        - 4x4 Use Case 2R, 4N

    US 05 Solve final question
        - 7x7 2K, 2Q, 2B, 1K
        - Time complexity 0(n)

REFERENCES
----------

    https://en.wikipedia.org/wiki/Chess#Movement
    https://en.wikipedia.org/wiki/Eight_queens_puzzle
    https://en.wikibooks.org/wiki/Algorithm_Implementation/Miscellaneous/N-Queens
    
