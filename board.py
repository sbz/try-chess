"""
sbz try-chess Python implementation
"""

class BoardOutofBoundsException(Exception):
    """ Out of bounds exception """
    pass

class Board(object):
    """
    Python class representing a board game matrix.
    """
    def __init__(self, x=3, y=3):
        """
        Constructor to build the game board
        """
        self.axis_x = x
        self.axis_y = y
        self.board = self.create()

    def create(self):
        """
        Create a empty board.
        """
        board_x = []

        for _ in range(self.axis_x):
            board_y = []
            for _ in range(self.axis_y):
                board_y.append('.')
            board_x.append(board_y)

        return board_x

    def place(self, piece):
        """
        Place a piece into the board.
        """
        if piece.pos_x > self.axis_x or piece.pos_y > self.axis_y or \
           piece.pos_x < 0 or piece.pos_y < 0:
            raise BoardOutofBoundsException("{} out of bounds".format(repr(piece)))
        self.board[piece.pos_x][piece.pos_y] = piece.symbol

    def display(self):
        """
        Display the board.
        """
        print str(self)

    def symetry(self):
        """
        Do symetry of the board.

        https://www.or-exchange.org/questions/3643/algorithm-for-symmetric-matrix-rearranging
        """
        raise NotImplementedError()

    def __str__(self):
        """
        Return the board into a string.
        """
        return "\n".join([str(line) for line in self.board])

    def __repr__(self):
        """
        String representation of the board.
        :return: string
        :rtype: str
        """
        return "<Board(x:{0},y:{1}>".format(self.axis_x, self.axis_y)
