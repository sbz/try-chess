#!/usr/bin/env python

from board import Board
from piece import King, Queen, Bishop, Rook, Knight

import random

def get_random_int(N):
    return random.randint(1, N) % N

if __name__ == '__main__':
    M = 7
    N = 7
    print("** Game start ***")
    chess = Board(M, N)
    chess.create()
    chess.display()
    print("** King attack move ***")
    king = King(get_random_int(M), get_random_int(M))
    chess.place(king)
    king.busy(chess)
    chess.display()
    print("*** Bishop attack move ***")
    bishop = Bishop(1, 1)
    chess = Board(N, M)
    chess.create()
    chess.place(bishop)
    bishop.busy(chess)
    chess.display()
    print("*** Rook attack move ***")
    rook = Rook(4, 4)
    chess = Board(N, M)
    chess.create()
    chess.place(rook)
    rook.busy(chess)
    chess.display()
    print("*** Knight attack move ***")
    knight = Knight(4, 2)
    chess = Board(N, M)
    chess.create()
    chess.place(knight)
    knight.busy(chess)
    chess.display()
    print("*** Queen attack move ***")
    queen = Queen(2, 2)
    chess = Board(N, M)
    chess.create()
    chess.place(queen)
    queen.busy(chess)
    chess.display()
