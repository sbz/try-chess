"""
Chess pieces classes
"""

class Piece(object):
    def __init__(self, pos_x=0, pos_y=0):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.symbol = '.'

    def __str__(self):
        return "{0}".format(self.symbol)

    def __repr__(self):
        class_name = self.__class__.__name__
        return "<{0}(x={1},y={2})>".format(class_name, self.pos_x,
                                          self.pos_y)
class King(Piece):
    def __init__(self, pos_x, pos_y):
        super(King, self).__init__(pos_x, pos_y)
        self.symbol = 'K'

    def busy(self, board):
        """
        Mark siblings cases as busy on board.
        """
        x, y = self.pos_x, self.pos_y
        board.board[x][y-1] = 'X'
        board.board[x][y+1] = 'X'
        board.board[x-1][y] = 'X'
        board.board[x+1][y] = 'X'
        board.board[x-1][y+1] = 'X'
        board.board[x-1][y-1] = 'X'
        board.board[x+1][y+1] = 'X'
        board.board[x+1][y-1] = 'X'

class Queen(Piece):
    def __init__(self, pos_x, pos_y):
        super(Queen, self).__init__(pos_x, pos_y)
        self.symbol = 'Q'

    def busy(self, board):
        """
        Queen move is the combine Rook and Bishop move
        """
        pos_x, pos_y = self.pos_x, self.pos_y
        rook = Rook(pos_x, pos_y)
        rook.busy(board)
        bishop = Bishop(pos_x, pos_y)
        bishop.busy(board, self.symbol)

class Bishop(Piece):
    def __init__(self, pos_x, pos_y):
        super(Bishop, self).__init__(pos_x, pos_y)
        self.symbol = 'B'

    def busy(self, board, sym='B'):
        """
        Mark siblings cases as busy on board.
        """
        pos_x = self.pos_x
        pos_y = self.pos_y
        self.busy_direction(-1, -1, pos_x, pos_y, board, sym)
        self.busy_direction(1, -1, pos_x, pos_y, board, sym)
        self.busy_direction(1, 1, pos_x, pos_y, board, sym)
        self.busy_direction(-1, 1, pos_x, pos_y, board, sym)

    def busy_direction(self, x_delta, y_delta, pos_x, pos_y, board, sym):
        for i in range(0, board.axis_x-1):
            x_new = pos_x + x_delta * i
            y_new = pos_y + y_delta * i
            if x_new < 0 or x_new >= board.axis_x \
               or y_new < 0 or y_new >= board.axis_y:
                continue
            if board.board[x_new][y_new] != sym:
                board.board[x_new][y_new] = 'X'

class Rook(Piece):
    def __init__(self, pos_x, pos_y):
        super(Rook, self).__init__(pos_x, pos_y)
        self.symbol = 'R'

    def busy(self, board):
        """
        Mark siblings cases as busy on board.
        """
        pos_x = self.pos_x
        pos_y = self.pos_y
        for i in range(0, pos_x):
            if i == pos_x:
                continue
            board.board[i][pos_y] = 'X'
        for i in range(pos_x, board.axis_x):
            if i == pos_x:
                continue
            board.board[i][pos_y] = 'X'
        for i in range(0, pos_y):
            if i == pos_y:
                continue
            board.board[pos_x][i] = 'X'
        for i in range(pos_y, board.axis_y):
            if i == pos_y:
                continue
            board.board[pos_x][i] = 'X'

class Knight(Piece):
    def __init__(self, pos_x, pos_y):
        super(Knight, self).__init__(pos_x, pos_y)
        self.symbol = 'N'

    def busy(self, board):
        """
        Mark siblings cases as busy on board.
        """
        x = self.pos_x
        y = self.pos_y
        board.board[x-2][y+1] = 'X'
        board.board[x-2][y-1] = 'X'
        board.board[x+2][y-1] = 'X'
        board.board[x+2][y+1] = 'X'
        board.board[x-1][y-2] = 'X'
        board.board[x+1][y-2] = 'X'
        board.board[x+1][y+2] = 'X'
        board.board[x-1][y+2] = 'X'
