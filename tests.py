import random
import unittest

from board import Board
from piece import King, Queen, Bishop, Rook, Knight

class BoardTestCase(unittest.TestCase):
    def setUp(self):
        self.x = 4
        self.y = 4
        self.chess = Board(self.x, self.y)

    def tearDown(self):
        del self.chess

    def test_board_create(self):
        ret = self.chess.create()
        self.assertIs(type([]), type(ret))
        self.assertIs(type([]), type(ret[0]))
        self.assertTrue(hasattr(self.chess,'board'))

class PieceTestCase(unittest.TestCase):

    def setUp(self):
        self.availables = [King, Queen, Bishop, Rook, Knight]
        self.piece = random.randint(0, 4)

    def tearDown(self):
        self.piece = None

    def test_position(self):
        piece = self.availables[self.piece](4, 4)
        self.assertEqual(piece.pos_x, 4)

    def test_symbol(self):
        self.assertEqual(King(1, 1).symbol, 'K')

if __name__ == '__main__':
    unittest.main()
